<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Services\AWS\S3Service;
use App\Services\Rebrandly\RebrandlyService;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        return view('posts.index', [
            'posts' => Post::all()
        ]);
    }

    public function create()
    {
        return view('posts.create');
    }

    public function store(
        Request $request,
        RebrandlyService $rebrandlyService,
        S3Service $s3Service
    ) {
        $post = new Post();
        $post->fill($request->except('_token'));
        $post->admin_id = $request->user()->id;
        $post->thumbnail = $s3Service->putObject(
            $request->thumbnail, S3Service::KEY_POSTS
        )['ObjectURL'];
        $post->reference = $rebrandlyService->shortUrl($request->reference) ?? $request->reference;
        $post->save();

        return redirect()->route('posts.edit', $post->id);
    }

    public function edit(Post $post)
    {
        return view('posts.edit', [
            'post' => $post
        ]);
    }

    public function update(
        Post $post,
        Request $request,
        RebrandlyService $rebrandlyService,
        S3Service $s3Service
    ) {
        $post->fill($request->except('_token', 'reference', 'thumbnail'));

        if (!is_null($request->reference)) {
            $post->reference = $rebrandlyService->shortUrl(
                $request->reference
            ) ?? $request->reference;
        }

        if (!is_null($request->thumbnail)) {
            $post->thumbnail = $s3Service->putObject(
                $request->thumbnail,
                S3Service::KEY_POSTS
            )['ObjectURL'];
        }

        $post->save();

        return redirect()->route('posts.edit', $post->id);
    }

    public function destroy(Post $post)
    {
        $post->delete();

        return redirect()->route('posts.index');
    }
}
