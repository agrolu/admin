<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        return view('admins.index', [
            'admins' => Admin::all()
        ]);
    }

    public function create()
    {
        return view('admins.create');
    }

    public function store(Request $request)
    {
        $admin = new Admin();
        $admin->fill($request->except('_token'));
        $admin->save();

        return redirect()->route('admins.edit', $admin->id);
    }

    public function edit(Admin $admin)
    {
        return view('admins.edit', [
            'admin' => $admin
        ]);
    }

    public function update(Admin $admin, Request $request)
    {
        $admin->fill($request->except('_token', 'password'));

        if (!is_null($request->password)) {
            $admin->password = $request->password;
        }

        $admin->save();

        return redirect()->route('admins.edit', $admin->id);
    }

    public function destroy(Admin $admin)
    {
        $admin->delete();

        return redirect()->route('admins.index');
    }
}
