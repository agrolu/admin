<?php

namespace App\Services\AWS;

use Aws\Laravel\AwsFacade as AWS;
use Illuminate\Http\UploadedFile;
use Ramsey\Uuid\Uuid;

class S3Service
{
    const KEY_POSTS = 'posts/';

    private $s3Client;

    public function __construct()
    {
        $this->s3Client = AWS::createClient('s3');
    }

    public function putObject(UploadedFile $file, $folder = '')
    {
        return $this->s3Client->putObject([
            'Bucket' => config('aws.S3.Bucket'),
            'Key' => $folder . Uuid::uuid4(),
            'Body' => get_bytes_image($file),
            'ACL' => 'public-read',
            'ContentType' => 'image/png'
        ]);
    }
}
