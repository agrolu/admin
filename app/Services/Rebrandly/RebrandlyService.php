<?php

namespace App\Services\Rebrandly;

use Illuminate\Support\Facades\Http;

class RebrandlyService
{
    public function shortUrl($url)
    {
        try {
            $response = Http::withHeaders([
                'Content-Type' => 'application/json',
                'apikey' => env('REBRANDLY_APP_KEY')
            ])->post('https://api.rebrandly.com/v1/links', [
                'destination' => $url,
                'domain' => [
                    'fullName' => 'rebrand.ly'
                ],
            ]);

            $response->throw();

            if (!isset($response['shortUrl'])) {
                throw new \Exception("attribute shortUrl not found");
            }

            return $response['shortUrl'];
        } catch (\Exception $exception) {
            return null;
        }
    }
}
