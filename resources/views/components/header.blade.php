<x-slot name="header">
    <div class="flex justify-between">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ $content }}
        </h2>
        @if (isset($route) && isset($redirect))
        <a href="{{ $route }}">{{ $redirect }}</a>
        @endif
    </div>
</x-slot>
