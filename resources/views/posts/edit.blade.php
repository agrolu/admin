<x-app-layout>
    <x-header :content="__('Editar Post - ' . $post->title)" />
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="mt-10 sm:mt-0">
                <div class="mt-5 md:mt-0 md:col-span-2">
                    <form action="{{ route('posts.update', $post->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="shadow overflow-hidden sm:rounded-md">
                            <div class="px-4 py-5 bg-white sm:p-6">
                                <div class="grid grid-cols-6 gap-6">
                                    <div class="col-span-6">
                                        <label for="title" class="block text-sm font-medium text-gray-700">Título</label>
                                        <input type="text" name="title" id="title" value="{{ $post->title }}" autocomplete="title" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                    </div>
                                    <div class="col-span-6">
                                        <label for="text" class="block text-sm font-medium text-gray-700">Texto</label>
                                        <textarea id="text" name="text" rows="3" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 mt-1 block w-full sm:text-sm border border-gray-300 rounded-md" placeholder="">{{ $post->text }}</textarea>
                                    </div>
                                    <div class="col-span-6">
                                        <label for="reference" class="block text-sm font-medium text-gray-700">Referência</label>
                                        <input type="text" name="reference" id="reference" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300" placeholder="{{ $post->reference }}">
                                    </div>
                                    <div class="col-span-6">
                                        <label
                                            class="
                                                w-64
                                                flex flex-col
                                                items-center
                                                px-4
                                                py-6
                                                bg-white
                                                rounded-md
                                                shadow-md
                                                tracking-wide
                                                uppercase
                                                border border-blue
                                                cursor-pointer
                                                hover:bg-purple-600 hover:text-white
                                                text-purple-600
                                                ease-linear
                                                transition-all
                                                duration-150
                                            "
                                            >
                                            <i class="fas fa-cloud-upload-alt fa-3x"></i>
                                            <span class="mt-2 text-base leading-normal">Selecione uma imagem</span>
                                            <input type="file" name="thumbnail" class="hidden" accept="image/*" />
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                                <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                Salvar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
